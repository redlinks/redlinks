��                �      �     �          	       
   0     ;  
   B     M     [     k  %   y     �  	   �  
   �  	   �     �     �     �          -     2     9  
   A     L     Y     ^     d     k  �  z      (  
   I     T     s     �     �     �     �     �     �     �                    %     3     B  -   `  #   �  	   �  
   �     �     �     �     �          	         %d rating %d ratings  Star %d rating %d ratings %s out of 5 stars Add review Cancel Course Id: Course Review Course comments Course review Display ratings and reviews of course Error Load More Loading... New title Pick up 1 course Please enter the review content Please enter the review title Please select your rating Rate Rating Reviews Show Rate: Show Review: Star Title Title: Write a review Project-Id-Version: LearnPress - Course Review
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-12-14 15:51+0700
PO-Revision-Date: 2018-10-04 21:56+0000
Last-Translator: adminweb_links <adminweb@redlinks.com.ec>
Language-Team: Español de Perú
Language: es_PE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Loco https://localise.biz/  %d Puntuación  %d Puntuaciones  Estrellas %d Puntuación %d Puntuaciones %s de 5 estrellas Agregar Puntuación Cancelar Local Reputación de Local Comentarios del local Puntuación de local Mostrar puntuaciones del local Error Cargar Más Cargando... Nuevo Título Elegir 1 local Por favor ingrese su opinión Por favor ingrese un título para su opinión Por favor, selecciona tu puntación Calificar Puntación Puntuaciones Mostrar puntaje: Mostrar opiniones: Estrella Título Título: Déjanos tu Opinión 