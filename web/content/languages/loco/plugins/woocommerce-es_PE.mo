��    �      �      	      	  @   	  -   ^	     �	  R   �	     �	     �	  !   �	     
     &
     2
     >
     U
     d
     
     �
     �
     �
     �
     �
     �
  &   �
          -     4  	   9     C     O     ]     f     t     y     �     �     �     �     �     �  	   �     �     �     �     �            
   '     2     >     C     I  '   f     �  	   �     �     �  	   �  
   �     �     �  "   �       "        >     Y  8   r     �     �     �     �     �     �                    (     7     =     D     K     _     g     z     �     �     �     �     �     �     �                    &     /     8     ?  	   D      N     o     v  	   �     �     �     �     �     �     �  
   �     �  	   �  	   �               /     B     V     k     �     �     �     �     �     �     �     �     �      �          '     7     =     D  
   P     [  	   `     j     x     �     �     �  
   �  �   �     K     Z  �  o  M   h  <   �     �  K   �     K     S  )   [     �     �     �     �  
   �     �     �  	   �  	   �     �          (     ;  (   O     x     �     �     �     �     �  	   �     �     �               $  	   *     4     T     \  
   i     t     �     �     �     �     �     �     �     �     �     �  '        <  
   M  
   X     c     r  	   {     �     �  $   �     �  $   �  -         .  i   M  
   �     �  
   �  
   �     �     �  	   �  
   �               3     :     B     J     `     i     �     �     �     �     �     �               %  	   ;     E  	   X     b     k     w     |  #   �     �     �  
   �     �     �     �          
          ,     >     L     U     g          �     �     �  $   �  $   
     /  	   7  
   A     L  
   c  	   n  	   x     �     �     �     �     �  
   �     �     �          	       	   '     1     :     B  	   Y  �   c     �        %s has been added to your cart. %s have been added to your cart. %s is a required field %s are required fields %s quantity <strong>%s product</strong> out of stock <strong>%s products</strong> out of stock Activate Add Add &ldquo;%s&rdquo; to your cart Add new tag Add to cart Add to menu Additional information Address line 1 Admin menu nameCategories Admin menu nameCoupons Admin menu nameProducts Admin menu nameTags All Products All categories All sources All tags Apartment, suite, unit etc. (optional) Billing details Cancel Cart Cart Page Cart totals Cart updated. Category Checkout Page City Company Company name Country County Coupon code already applied! Day(s) Description Discount: Edit tag Email address Email address. Filter Filter by product Filter by source First name First name. Free Free! House number and street name I have read and agree to the website %s Invalid coupon Inventory Items Last access Last name Last name. List of categories. Low in stock Make a duplicate from this product Month(s) Move &#8220;%s&#8221; to the Trash No products found in range No products in the cart. Notes about your order, e.g. special notes for delivery. Order notes Out of stock Page settingCart Page slugcart Page slugcheckout Page titleCart Phone Phone number. Place order Postcode / ZIP Price Price: Prices Proceed to checkout Product Product Categories Product Title Product Type Product categories Product description Product gallery Product name Product search Product short description Product tags Products Products list Province Quantity Rating Read Read more Read more about &ldquo;%s&rdquo; Ready! Related products Relevance Return to cart Reviews Reviews (%d) Sale! Search categories Search tags Select all Select none Shipping: Shop Page Sort by average rating Sort by most recent Sort by popularity Sort by price (asc) Sort by price (desc) Sort by price: high to low Sort by price: low to high Source State / County Stock Stock quantity Street address Tag Taxes Terms and conditions The %s has already been deleted. Town / City Town / District Trash Update Update cart Update tag User View cart View products Week(s) Write Year(s) Your cart is currently empty. Your order Your personal data will be used to process your order, support your experience throughout this website, and for other purposes described in our %s. privacy policy terms and conditions Project-Id-Version: WooCommerce 3.4.5
Report-Msgid-Bugs-To: https://github.com/woocommerce/woocommerce/issues
POT-Creation-Date: 2019-05-27 18:49:33+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2019-06-26 19:33+0000
Last-Translator: Administrador <adminweb@redlinks.com.ec>
Language-Team: Español de Perú
X-Generator: Loco https://localise.biz/
Language: es_PE
Plural-Forms: nplurals=2; plural=n != 1;
X-Loco-Version: 2.3.0; wp-4.9.8 %s ha sido añadido a tu cotización.  %s han sido añadido a tu cotización. %s este es un campo requerido %s estos campos son requeridos %s cantidad <strong>%s producto</strong> agotado <strong>%s productos</strong> agotados Activar Agregar Agregar &ldquo;%s&rdquo; a la cotización Agregar nueva etiqueta Cotizar Agregar al menú Información Adicional Dirección Categorías Cupones Productos Etiquetas Todos los Productos Todas las categorias Todos los recursos Todas las etiquetas Número de inmueble u oficina (opcional) Información de Contacto Cancelar Cotización Página de Cotización Cotización Pedido Actualizado. Categoria Página de Cotización Ciudad Empresa Empresa o Institución País Provincia Este cupón ya ha sido aplicado Día(s) Descripción Descuento: Editar etiquetas Dirección Email Dirección Email. Filtro Filtrar por producto Filtrar por recurso Nombre Nombre. Gratis Gratis! Calle principal y secundaria  He leído y estoy de acuerdo con los %s Cupón Inválido Inventario Artículos Último Acceso Apellido Apellido. Lista de Categorías. Bajo en inventario Hacer un duplicado de este productos Mes(es) Mover &#8220;%s&#8221; a la papelera No se encontraron productos en esta búsqueda No hay productos para cotizar. Escribe aquí cualquier indicación adicional que nos permita cotizar tu pedido de forma más específica Importante Agotado Cotizacion cotizacion pedido Cotización Teléfono Teléfono. Solicitar Cotización Código Postal / ZIP Precio Precio: Precios Solicitar Cotización Producto Categorías de Productos Titulo de Producto Tipo de Producto Categorías de Productos Descripción del Producto Galería de Productos Nombre de Producto Buscar Producto Descripción Corta Etiqueta de Productos Productos Lista de Productos Provincia Cantidad Reputación Leer Ver más Ver más acerca de &ldquo;%s&rdquo; Listo! Otros Productos Relevancia Regresar a la cotización Puntuaciones Puntuaciones (%d) Promo Buscar categoria Buscar Etiquetas Seleccionar todos Deseleccionar Entrega: Página de tienda Ordenar por reputación Ordenar por más recientes Ordenar por popularidad Ordenar por precio (asc) Ordenar por precio (desc) Ordenar por precio: de mayor a menor Ordenar por precio: de menor a mayor Fuentes Provincia Inventario Cantidad en Inventario Dirección Etiquetas Impuestos Términos y condiciones El %s ha sido eliminado. Ciudad Ciudad Papelera Actualizar Actualizar Cotización Adtualizar etiquetas Usuario Ver cotización Ver Productos Semana(s) Escribir Año(s) Su pedido está vacío Su pedido Declaro que la información de contacto es verás y que deseo recibir vía email o telefónica cotizaciones y comunicados detallados en la siguiente %s. Políticas de privacidad términos y condiciones 