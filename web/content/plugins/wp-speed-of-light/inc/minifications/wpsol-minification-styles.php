<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

/**
 * Class WpsolMinificationStyles
 */
class WpsolMinificationStyles extends WpsolMinificationBase
{
    /**
     * Init minify css params
     *
     * @var boolean
     */
    private $minifyCSS = false;
    /**
     * Init css params
     *
     * @var array
     */
    private $css = array();
    /**
     * Init css code params
     *
     * @var array
     */
    private $csscode = array();
    /**
     * Init defer params
     *
     * @var boolean
     */
    private $defer = false;
    /**
     * Init default exclude params
     *
     * @var array
     */
    private $defaultExcludeCSS = array();
    /**
     * Init css inline size params
     *
     * @var string
     */
    private $cssinlinesize = '';
    /**
     * Init google font params
     *
     * @var array
     */
    private $grfonts = array('fonts.googleapis.com');
    /**
     * Init group fonts params
     *
     * @var boolean
     */
    private $group_fonts = false;
    /**
     * Init include inline params
     *
     * @var boolean
     */
    private $include_inline = false;
    /**
     * Init group css params
     *
     * @var boolean
     */
    private $group_css = false;
    /**
     * Init css min array params
     *
     * @var array
     */
    private $css_min_arr = array();
    /**
     * Init minified url params
     *
     * @var boolean
     */
    private $minified_url = array();
    /**
     * Init css exclude params
     *
     * @var array
     */
    private $css_exclude = array();

    /**
     * Reads the page and collects style tags
     *
     * @param array $options Option of minify css
     *
     * @return boolean
     */
    public function read($options)
    {
        /**
         * Should we minify the specified inline css content
         *
         * @param true Default value
         * @param string Css content
         *
         * @return boolean
         */
        $dominify = apply_filters('wpsol_css_inline_do_minify', true, $this->content);
        if (!$dominify) {
            return false;
        }

        /**
         * Apply filter inline size of css
         *
         * @param string wpsol_css_inline_max_size
         * @param integer Default value
         *
         * @return integer
         */
        $this->cssinlinesize = apply_filters('wpsol_css_inline_max_size', 256);

        $this->minifyCSS = $options['minifyCSS'];

        // group css?
        $this->group_css = $options['groupcss'];

        $this->group_fonts = $options['groupfonts'];

        //custom css,font exclude
        $this->css_exclude = $options['exclude_css'];

        // what CSS shouldn't be exclude
        $this->defaultExcludeCSS = $options['css_exclude'];

        // should we defer css?
        $this->defer = $options['defer'];

        // include inline?
        $this->include_inline = $options['include_inline'];

        // exclude (no)script, as those may contain CSS which should be left as is
        if (strpos($this->content, '<script') !== false) {
            $this->content = preg_replace_callback(
                '#<(?:no)?script.*?<\/(?:no)?script>#is',
                function ($matches) {
                    return '%%SCRIPT' . WPSOL_HASH . '%%' . base64_encode($matches[0]) . '%%SCRIPT%%';
                },
                $this->content
            );
        }

        // Hide comments
        $this->content = $this->hideComments($this->content);
        // Hide IE hacks
        $this->content = $this->hideIEhacks($this->content);

        // Get <style> and <link>
        if (preg_match_all('#(<style[^>]*>.*</style>)|(<link[^>]*stylesheet[^>]*>)#Usmi', $this->content, $matches)) {
            foreach ($matches[0] as $tag) {
                if ($this->isMatch($tag, $this->defaultExcludeCSS)) {
                    continue;
                }
                if ($this->isMatch($tag, $this->css_exclude)) {
                    //Exclude css by customer
                    continue;
                }

                // Get the media
                if (strpos($tag, 'media=') !== false) {
                    preg_match('#media=(?:"|\')([^>]*)(?:"|\')#Ui', $tag, $medias);
                    $medias = explode(',', $medias[1]);
                    $media = array();
                    foreach ($medias as $elem) {
                        if (empty($elem)) {
                            $elem = 'all';
                        }
                        $media[] = $elem;
                    }
                } else {
                    // No media specified - applies to all
                    $media = array('all');
                }

                //Check exclude style with media from group css
                if ($this->group_css) {
                    if ((count($media) === 1 && !in_array('all', $media)) || (count($media) > 1)) {
                        continue;
                    }
                }

                if (preg_match('#<link.*href=("|\')(.*)("|\')#Usmi', $tag, $source)) {
                    if ($this->group_fonts && $this->isMatch($tag, $this->grfonts)) {
                        // google font link
                        $this->css[] = array($media, 'GOOGLEFONT;'.$source[2]);
                        $this->content = str_replace($tag, '', $this->content);
                        continue;
                    }
                    // <link>
                    $url = current(explode('?', $source[2], 2));
                    $path = $this->getpath($url);

                    if ($path !== false && preg_match('#\.css$#', $path)) {
                        // Good link
                        $this->css[] = array($media, $path);
                        // Remove the original style tag
                        $this->content = str_replace($tag, '', $this->content);
                    }
                } else {
                    // inline css in style tags can be wrapped
                    // Restore comment inline scripts
                    $tag = $this->restoreComments($tag);
                    if (preg_match('#<style.*>(.*)</style>#Usmi', $tag, $code)) {
                        if ($this->include_inline) {
                            $regex = '#^.*<!\[CDATA\[(?:\s*\*/)?(.*)(?://|/\*)\s*?\]\]>.*$#sm';
                            $code = preg_replace($regex, '$1', $code[1]);
                            // Font check
                            $font_face = array('@font-face');
                            if (empty($code) || (!$this->group_fonts && $this->isMatch($code, $font_face))) {
                                continue;
                            }
                            $this->css[] = array($media, 'INLINE;' . $code);
                            // Remove the original style tag
                            $this->content = str_replace($tag, '', $this->content);
                        }
                    }
                }
            }
            return true;
        }
        // Really, no styles?
        return false;
    }

    /**
     * Joins and optimizes CSS
     *
     * @return boolean
     */
    public function minify()
    {
        foreach ($this->css as $group) {
            list($media, $css) = $group;
            $csscode = '';
            if (preg_match('#^INLINE;#', $css)) {
                // <INLINE style>
                $css = preg_replace('#^INLINE;#', '', $css);
                $csscode = $this->fixurls(ABSPATH . '/index.php', $css);
            } elseif (preg_match('#^GOOGLEFONT;#', $css)) {
                $css = preg_replace('#^GOOGLEFONT;#', '', $css);
                if (strpos($css, '//') === 0) {
                    if (is_ssl()) {
                        $http = 'https:';
                    } else {
                        $http = 'http:';
                    }
                    $css = $http . $css;
                }
                $verify_peer = array(
                    'verify_peer'      => false,
                    'verify_peer_name' => false
                );
                // check ssl verify
                $streamContext = stream_context_create(
                    array('ssl' => $verify_peer)
                );
                //get css from server
                $csscode = file_get_contents($css, false, $streamContext);
            } else {
                /**
                 * Apply filter to allow or not minifiying a css url
                 *
                 * @param boolean Default check minify value
                 * @param string  Style url
                 *
                 * @return boolean|string
                 */
                $minify_this_css = apply_filters('wpsol_css_url_do_minify', true, $css);
                if (!$minify_this_css) {
                    continue;
                }
                //<link>
                if ($css !== false && file_exists($css) && is_readable($css)) {
                    $csscode = file_get_contents($css);
                    $csscode = $this->fixurls($css, $csscode);
                    $csscode = preg_replace('/\x{EF}\x{BB}\x{BF}/', '', $csscode);

                    if (strpos($css, 'min.css') !== false) {
                        $csscode = '/*MINIFIED;*/'.$csscode;
                    }
                }
            }

            $this->csscode[] = array($media, $csscode);
        }

        if (!empty($this->csscode)) {
            if ($this->group_css) {
                // Ready minified css
                $md5hash = '';
                $media_group = array('all');
                foreach ($this->csscode as $group) {
                    list($media, $csscode) = $group;
                    $md5hash .= $csscode;
                }

                $hash = md5($md5hash);

                // Check for already-minified code
                $cacheMinify = new WpsolMinificationCache($hash, 'css');
                if ($cacheMinify->check()) {
                    $cssCacheExist = $cacheMinify->retrieve();

                    $this->css_min_arr[] = array($media_group, $hash, $cssCacheExist);
                    return true;
                }
                unset($cacheMinify);

                // Minify
                $groupCode = '';
                foreach ($this->csscode as $group) {
                    list($media, $csscode) = $group;
                    // Minify
                    if (!preg_match('#^MINIFIED;#', $csscode)) {
                        if (class_exists('Minify_CSS_Compressor')) {
                            $tmp_code = trim(Minify_CSS_Compressor::process($csscode));
                        } elseif (class_exists('CSSmin')) {
                            $cssmin = new CSSmin();
                            if (method_exists($cssmin, 'run')) {
                                $tmp_code = trim($cssmin->run($csscode));
                            } elseif (is_callable(array($cssmin, 'minify'))) {
                                $tmp_code = trim(CssMin::minify($csscode));
                            }
                        }
                        if (!empty($tmp_code)) {
                            $csscode = '/*WPSOL_MINIFIED_CSS*/'.$tmp_code;
                            unset($tmp_code);
                        }
                    }

                    $groupCode .= '/*WPSOL_GROUP_CSS*/'. $csscode . "\n";
                }

                $this->css_min_arr[] = array($media_group, $hash, $groupCode);
            } else {
                foreach ($this->csscode as $group) {
                    list($media, $csscode) = $group;

                    $hash = md5($csscode);
                    $cacheMinify = new WpsolMinificationCache($hash, 'css');
                    if ($cacheMinify->check()) {
                        $cssCacheExist = $cacheMinify->retrieve();
                        $this->css_min_arr[] = array($media, $hash, $cssCacheExist);
                        continue;
                    }
                    unset($cacheMinify);

                    // Minify
                    if (!preg_match('#^MINIFIED;#', $csscode)) {
                        if (class_exists('Minify_CSS_Compressor')) {
                            $tmp_code = trim(Minify_CSS_Compressor::process($csscode));
                        } elseif (class_exists('CSSmin')) {
                            $cssmin = new CSSmin();
                            if (method_exists($cssmin, 'run')) {
                                $tmp_code = trim($cssmin->run($csscode));
                            } elseif (is_callable(array($cssmin, 'minify'))) {
                                $tmp_code = trim(CssMin::minify($csscode));
                            }
                        }
                        if (!empty($tmp_code)) {
                            $csscode = '/*WPSOL_MINIFIED_CSS*/'.$tmp_code;
                            unset($tmp_code);
                        }
                    }

                    $this->css_min_arr[] = array($media, $hash, $csscode);
                }
            }
        }

        return true;
    }

    /**
     *  Caches the CSS in uncompressed, deflated and gzipped form.
     *
     * @return void
     */
    public function cache()
    {
        if (!empty($this->css_min_arr)) {
            foreach ($this->css_min_arr as $group) {
                list($media, $hash, $csscode) = $group;

                $cacheMinify = new WpsolMinificationCache($hash, 'css');
                if (!$cacheMinify->check()) {
                    // Cache our code
                    $cacheMinify->cache($csscode, 'text/css');
                }

                $url = WPSOL_CACHE_URL . $cacheMinify->getname();

                $this->minified_url[] = array($media, $hash, $url);
            }
        }
    }

    /**
     * Returns the content
     *
     * @return mixed|string
     */
    public function getcontent()
    {
        // restore (no)script
        if (strpos($this->content, '%%SCRIPT%%') !== false) {
            $this->content = preg_replace_callback(
                '#%%SCRIPT' . WPSOL_HASH . '%%(.*?)%%SCRIPT%%#is',
                function ($matches) {
                    return base64_decode($matches[1]);
                },
                $this->content
            );
        }

        $replaceTag = array('<title', 'before');

        if ($this->defer) {
            $deferredCssBlock = "<script data-cfasync='false'>function lCss(url,media) {var d=document;var l=d.createElement('link');";
            // phpcs:ignore WordPress.WP.EnqueuedResources -- This is writing direct style to the content
            $deferredCssBlock .= "l.rel='stylesheet';l.type='text/css';l.href=url;l.media=media;aoin=d.getElementsByTagName('noscript')[0];aoin.parentNode.insertBefore(l,aoin.nextSibling);}";
            $deferredCssBlock .= 'function deferredCSS() {';
            $noScriptCssBlock = '<noscript>';
        }

        if (!empty($this->minified_url)) {
            foreach ($this->minified_url as $group) {
                list($media, $hash, $url) = $group;

                $media_ele = '';
                if (!empty($media)) {
                    $media_ele = implode(',', $media);
                }

                $cacheMinify = new WpsolMinificationCache($hash, 'css');
                if ($cacheMinify->check()) {
                    $csscode = $cacheMinify->retrieve();
                }

                //Add the stylesheet either deferred (import at bottom) or normal links in head
                if ($this->defer) {
                    $deferredCssBlock .= "lCss('" . $url . "','" . $media_ele . "');";
                    $noScriptCssBlock .= '<link type="text/css" media="' . $media_ele;
                    // phpcs:ignore WordPress.WP.EnqueuedResources -- This is writing direct style to the content
                    $noScriptCssBlock .= '" href="' . $url . '" rel="stylesheet" />';
                } else {
                    if (strlen($csscode) > $this->cssinlinesize) {
                        $minified = '<link type="text/css" media="' . $media_ele . '" href="';
                        // phpcs:ignore WordPress.WP.EnqueuedResources -- This is writing direct style to the content
                        $minified .= $url . '" rel="stylesheet" />';
                    } elseif (strlen($csscode) > 0) {
                        $minified = '<style type="text/css" media="' . $media_ele . '">' . $csscode . '</style>';
                    }
                    $this->injectInHtml($minified, $replaceTag);
                }
            }
        }

        if ($this->defer) {
            $deferredCssBlock .= "}if(window.addEventListener){window.addEventListener('DOMContentLoaded',deferredCSS,false);}else{window.onload = deferredCSS;}</script>";
            $noScriptCssBlock .= '</noscript>';
            $this->injectInHtml($noScriptCssBlock, $replaceTag);
            $this->injectInHtml($deferredCssBlock, array('</body>', 'before'));
        }

        // Restore COmments
        $this->content = $this->restoreComments($this->content);
        // Restore IE hacks
        $this->content = $this->restoreIEhacks($this->content);

        //Return the modified stylesheet
        return $this->content;
    }


    /**
     * Fix urls to avoid breaking URLs
     *
     * @param string $file Url of file
     * @param string $code Css code
     *
     * @return mixed
     */
    public static function fixurls($file, $code)
    {
        $file = str_replace(WPSOL_ROOT_DIR, '/', $file);
        $dir = dirname($file); //Like /wp-content

        // quick fix for import-troubles in e.g. arras theme
        $code = preg_replace('#@import ("|\')(.+?)\.css("|\')#', '@import url("${2}.css")', $code);

        if (preg_match_all('#url\((?!data)(?!\#)(?!"\#)(.*)\)#Usi', $code, $matches)) {
            $replace = array();
            foreach ($matches[1] as $k => $url) {
                // Remove quotes
                $url = trim($url, " \t\n\r\0\x0B\"'");
                $noQurl = trim($url, "\"'");
                if ($url !== $noQurl) {
                    $removedQuotes = true;
                } else {
                    $removedQuotes = false;
                }
                $url = $noQurl;
                if (substr($url, 0, 1) === '/' || preg_match('#^(https?://|ftp://|data:)#i', $url)) {
                    //URL is absolute
                    continue;
                } else {
                    // relative URL
                    $str_replace = str_replace('//', '/', $dir . '/' . $url);
                    $subject = str_replace(' ', '%20', WPSOL_WP_ROOT_URL . $str_replace);
                    $newurl = preg_replace('/https?:/', '', $subject);

                    $hash = md5($url);
                    $code = str_replace($matches[0][$k], $hash, $code);

                    if (!empty($removedQuotes)) {
                        $replace[$hash] = 'url(\'' . $newurl . '\')';
                    } else {
                        $replace[$hash] = 'url(' . $newurl . ')';
                    }
                }
            }
            //Do the replacing here to avoid breaking URLs
            $code = str_replace(array_keys($replace), array_values($replace), $code);
        }
        return $code;
    }
}
