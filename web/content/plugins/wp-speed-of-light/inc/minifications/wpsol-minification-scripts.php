<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

/**
 * Class WpsolMinificationScripts
 */
class WpsolMinificationScripts extends WpsolMinificationBase
{
    /**
     * Init minify javascript params
     *
     * @var boolean
     */
    private $minifyJS = false;
    /**
     * Init script params
     *
     * @var array
     */
    private $scripts = array();
    /**
     * Init dont move params
     *
     * @var array
     */
    private $default_exclude = array('document.write', 'html5.js', 'show_ads.js', 'google_ad',
        'blogcatalog.com/w', 'tweetmeme.com/i', 'mybloglog.com/', 'histats.com/js', 'ads.smowtion.com/ad.js',
        'statcounter.com/counter/counter.js', 'widgets.amung.us', 'ws.amazon.com/widgets', 'media.fastclick.net',
        '/ads/', 'comment-form-quicktags/quicktags.php', 'edToolbar', 'intensedebate.com', 'scripts.chitika.net/',
        '_gaq.push', 'jotform.com/', 'admin-bar.min.js', 'GoogleAnalyticsObject', 'plupload.full.min.js',
        'syntaxhighlighter', 'adsbygoogle', 'gist.github.com', '_stq', 'nonce', 'post_id', 'data-noptimize','addthis.com',
        '/afsonline/show_afs_search.js', 'disqus.js', 'networkedblogs.com/getnetworkwidget', 'infolinks.com/js/',
        'jd.gallery.js.php', 'jd.gallery.transitions.js', 'swfobject.embedSWF(', 'linkwithin.com/widget.js',
        'tiny_mce.js', 'tinyMCEPreInit.go','gaJsHost', 'load_cmc', 'jd.gallery.transitions.js',
        'swfobject.embedSWF(', 'tiny_mce.js', 'tinyMCEPreInit.go','s_sid', 'smowtion_size', 'sc_project', 'WAU_',
        'wau_add', 'comment-form-quicktags', 'edToolbar', 'ch_client', 'seal.js', 'maps.googleapis.com', 'js.stripe.com', 'lodgix.com');
    /**
     * Init minified url params
     *
     * @var string
     */
    private $minified_url = '';
    /**
     * Init md5hash params
     *
     * @var string
     */
    private $md5hash = '';
    /**
     * Init group js params
     *
     * @var boolean
     */
    private $group_js = false;
    /**
     * Init javascript group value params
     *
     * @var array
     */
    private $js_val = array();
    /**
     * Init javascript min  params
     *
     * @var array
     */
    private $js_min_arr = array();
    /**
     * Init url group params
     *
     * @var array
     */
    private $url_arr = array();
    /**
     * Init javascript exclude params
     *
     * @var array
     */
    private $js_exclude = array();
    /**
     * Init external scripts params
     *
     * @var array
     */
    private $external_scripts = array();
    /**
     * Init external local path params
     *
     * @var array
     */
    private $external_local_url = array();
    /**
     * Init js after group params
     *
     * @var string
     */
    private $js_after_group = '';
    /**
     * Init cache external params
     *
     * @var boolean
     */
    private $cache_external = false;
    /**
     * Init exclude inline params
     *
     * @var boolean
     */
    private $exclude_inline = false;
    /**
     * Init move to footer params
     *
     * @var boolean
     */
    private $move_to_footer = false;
    /**
     * Init all script compare params
     *
     * @var array
     */
    private $script_to_footer = array();
    /**
     * Init exclude move to footer params
     *
     * @var array
     */
    private $exclude_move_to_footer = array();
    /**
     * Reads the page and collects script tags
     *
     * @param array $options Option of minify js
     *
     * @return boolean
     */
    public function read($options)
    {
        //turn on minification
        $this->minifyJS = $options['minify_js'];
        // group js?
        $this->group_js = $options['group_js'];

        //cache external js
        $this->cache_external = $options['cache_external'];
        //exclude inline script
        $this->exclude_inline = $options['exclude_inline'];
        //exclude inline script
        $this->move_to_footer = $options['move_to_script'];
        //exclude inline script
        $this->exclude_move_to_footer = $options['exclude_move_to_script'];
        // get extra exclusions settings or filter
        $this->js_exclude = array_merge($options['exclude_js'], $this->default_exclude);

        // Hide comments
        $this->content = $this->hideComments($this->content);
        // Hide IE hacks
        $this->content = $this->hideIEhacks($this->content);

        //Get script files
        if (preg_match_all('#<script.*</script>#Usmi', $this->content, $matches)) {
            foreach ($matches[0] as $tag) {
                // Determines wheter a <script> $tag should be aggregated or not.
                if (!$this->shouldMinify($tag)) {
                    continue;
                }

                if ($this->isMatch($tag, $this->js_exclude)) {
                    //Exclude css by customer
                    continue;
                }

                if (preg_match('#src=("|\')(.*)("|\')#Usmi', $tag, $source)) {
                    // External script
                    $url = current(explode('?', $source[2], 2));
                    $path = $this->getpath($url);

                    if ($path !== false && preg_match('#\.js$#', $path)) {
                        // Set url to compare for move to footer
                        $this->script_to_footer[$tag] = $url;

                        //We can merge it
                        $this->scripts[$tag] = $path;
                    } else {
                        //External script (example: google analytics)
                        if ($this->cache_external) {
                            $this->external_scripts[$tag] = $url;
                        }
                    }
                } else {
                    // Inline script
                    // Set url to compare for move to footer
                    // Restore comment inline scripts
                    $tag = $this->restoreComments($tag);
                    if (preg_match('#<script.*>(.*)</script>#Usmi', $tag, $match)) {
                        $code = preg_replace('#.*<!\[CDATA\[(?:\s*\*/)?(.*)(?://|/\*)\s*?\]\]>.*#sm', '$1', $match[1]);
                        $code = preg_replace('/(?:^\\s*<!--\\s*|\\s*(?:\\/\\/)?\\s*-->\\s*$)/', '', $code);
                        if (empty($code)) {
                            continue;
                        }
                        if (!$this->exclude_inline) {
                            // Set inline to compare for move to footer
                            $this->script_to_footer[$tag] =  $code;
                            // We can minify inline
                            $this->scripts[$tag] = 'INLINE;' . $code;
                        }
                    }
                }
            }
            return true;
        }

        // No script files, great ;-)
        return false;
    }

    /**
     * Joins and optimizes JS
     *
     * @return boolean
     */
    public function minify()
    {
        foreach ($this->scripts as $tag => $script_in) {
            $script = '/*WPSOL_NOT_CONTENT*/';
            /**
             * Should we minify the specified javascript file
             * The filter should return true to minify the file or false if it should not be minified
             *
             * @param boolean Default check minify value
             * @param string  Script url
             *
             * @return boolean
             */
            $check_allow_minify = apply_filters('wpsol_js_url_do_minify', true, $script_in);
            //Check allow minify
            if (!$check_allow_minify) {
                continue;
            }

            if (preg_match('#^INLINE;#', $script_in)) {
                //Inline script
                $script = preg_replace('#^INLINE;#', '', $script_in);
            } else {
                //External script
                if ($script_in !== false && file_exists($script_in) && is_readable($script_in)) {
                    $script = file_get_contents($script_in);
                }
            }
            $script = preg_replace('/\x{EF}\x{BB}\x{BF}/', '', $script);
            $script = rtrim($script, ";\n\t\r") . ';';

            $this->js_val[$tag] = $script;
        }

        if (!empty($this->js_val)) {
            if ($this->group_js) {
                $hashname = '';
                $groupCode = '';
                foreach ($this->js_val as $tag => $jscode) {
                    $hashname .= $jscode;
                    //Remove all old script to group
                    if (strpos($jscode, 'WPSOL_NOT_CONTENT') === false) {
                        // Check if can not get script content, it will be not remove
                        $this->content = str_replace($tag, '', $this->content);
                    }
                }

                $this->md5hash = md5($hashname);

                //Check for already-minified code
                $cacheMinify = new WpsolMinificationCache($this->md5hash, 'js');
                if ($cacheMinify->check()) {
                    $this->js_after_group = $cacheMinify->retrieve();
                    return true;
                }
                unset($cacheMinify);

                foreach ($this->js_val as $tag => $jscode) {
                    if (strpos($jscode, 'WPSOL_NOT_CONTENT') !== false) {
                        continue;
                    }
                    if (strpos($tag, 'min.js') === false) {
                        if ($this->minifyJS && class_exists('JSMin')) {
                            if (is_callable(array('JSMin', 'minify'))) {
                                $tmp_jscode = trim(JSMin::minify($jscode));
                                if (!empty($tmp_jscode)) {
                                    $jscode = '/*WPSOL_MINIFIED_JS*/'.$tmp_jscode;
                                    unset($tmp_jscode);
                                }
                            }
                        }
                    }

                    $groupCode .= '/*WPSOL_GROUP_JS*/'. $jscode . "\n";
                }

                $this->js_after_group = $groupCode;
            } else {
                foreach ($this->js_val as $tag => $jscode) {
                    if (strpos($tag, 'min.js') !== false) {
                        continue;
                    }
                    if (strpos($jscode, 'WPSOL_NOT_CONTENT') !== false) {
                        continue;
                    }

                    $hash = md5($jscode);

                    //Check for already-minified code
                    $cacheMinify = new WpsolMinificationCache($hash, 'js');
                    if ($cacheMinify->check()) {
                        $jscodeExist = $cacheMinify->retrieve();
                        $this->js_min_arr[] = array($hash, $tag, $jscodeExist);
                        continue;
                    }
                    unset($cacheMinify);

                    // Minify
                    if (class_exists('JSMin')) {
                        if (is_callable(array('JSMin', 'minify'))) {
                            $tmp_jscode = trim(JSMin::minify($jscode));
                            if (!empty($tmp_jscode)) {
                                $jscode = '/*WPSOL_MINIFIED_JS*/'.$tmp_jscode;
                                unset($tmp_jscode);
                            }
                        }
                    }

                    $this->js_min_arr[] = array($hash, $tag, $jscode);
                }
            }

            return true;
        }

        return false;
    }

    /**
     * Caches the JS in uncompressed, deflated and gzipped form.
     *
     * @return void
     */
    public function cache()
    {
        if ($this->group_js) {
            if (!empty($this->js_after_group)) {
                $cache = new WpsolMinificationCache($this->md5hash, 'js');
                if (!$cache->check()) {
                    //Cache our code
                    $cache->cache($this->js_after_group, 'text/javascript');
                }

                $this->minified_url = WPSOL_CACHE_URL . $cache->getname();
            }
        } else {
            if (!empty($this->js_min_arr)) {
                foreach ($this->js_min_arr as $group) {
                    list($hash, $tag, $js_minified) = $group;

                    $cacheMinify = new WpsolMinificationCache($hash, 'js');
                    if (!$cacheMinify->check()) {
                        //Cache our code
                        $cacheMinify->cache($js_minified, 'text/javascript');
                    }
                    $url = WPSOL_CACHE_URL . $cacheMinify->getname();

                    $this->url_arr[] = array($hash, $tag, $url);
                }
            }
        }

        if ($this->cache_external) {
            // Cache external script
            if (!empty($this->external_scripts)) {
                foreach ($this->external_scripts as $tag => $url) {
                    $script = $this->getExternalData($url);
                    if (empty($script) || strpos($script, 'get content remotely failed') !== false) {
                        continue;
                    }
                    $hash = md5($script);

                    $cacheMinify = new WpsolMinificationCache($hash, 'js');
                    if (!$cacheMinify->check()) {
                        //Cache external code
                        $cacheMinify->cache($script, 'text/javascript');
                    }

                    $cache_external_url = WPSOL_CACHE_URL . $cacheMinify->getname();

                    $this->external_local_url[] = array($tag, $cache_external_url);
                }
            }
        }
    }

    /**
     * Returns the content
     *
     * @return mixed|string
     */
    public function getcontent()
    {
        // Add the scripts taking forcehead/ deferred (default) into account
        $defer = '';
        $async = '';
        if ($this->move_to_footer) {
            $replaceTag = array('</body>', 'before');
        } else {
            $replaceTag = array('</head>', 'before');
        }


        if ($this->group_js) {
            $defer = 'defer ';
            $minified = '<script type="text/javascript" ' . $defer ;
            $minified .= $async . 'src="' . $this->minified_url . '"></script>';
            $this->injectInHtml($minified, $replaceTag);
        } else {
            if ($this->minifyJS) {
                if ($this->move_to_footer) {
                    foreach ($this->url_arr as $group) {
                        list($hash, $tag, $url) = $group;

                        if (array_key_exists($tag, $this->script_to_footer)) {
                            $this->script_to_footer[$tag] = $hash.'_wpsolmtf_'.$url;
                        }
                    }

                    foreach ($this->script_to_footer as $tag => $script) {
                        if (strpos($script, '_wpsolmtf_') !== false) {
                            $hash = substr($script, 0, strpos($script, '_wpsolmtf_'));
                            $url = substr($script, strpos($script, '_wpsolmtf_') + strlen('_wpsolmtf_'));

                            if (preg_match('#src=("|\')(.*)("|\')#Usmi', $tag, $source)) {
                                $script_to_footer = '<script type="text/javascript" '.$async.$defer . 'src="' . $url . '"></script>';
                            } else {
                                $cacheMinify = new WpsolMinificationCache($hash, 'js');
                                if ($cacheMinify->check()) {
                                    $inline_script = $cacheMinify->retrieve();
                                }

                                if (strlen($inline_script) > 0) {
                                    $script_to_footer = '<script type="text/javascript" '.$async.$defer.'>'.$inline_script.'</script>';
                                } else {
                                    $script_to_footer = '<script type="text/javascript" '.$async . $defer.'src="'.$url.'"></script>';
                                }
                            }
                        } else {
                            if (preg_match('#src=("|\')(.*)("|\')#Usmi', $tag, $source)) {
                                $script_to_footer = '<script type="text/javascript" '.$async.$defer . 'src="' . $script . '"></script>';
                            } else {
                                $script_to_footer = '<script type="text/javascript" ' . $async . $defer . '>'.$script.'</script>';
                            }
                        }
                        // Exclude script from "move to footer"
                        if ($this->isMatch($tag, $this->exclude_move_to_footer)) {
                            $this->content = str_replace($tag, $script_to_footer, $this->content);
                        } else {
                            // Remove old script
                            $this->content = str_replace($tag, '', $this->content);
                            // Inject script to footer
                            $this->injectInHtml($script_to_footer, $replaceTag);
                        }
                    }
                } else {
                    foreach ($this->url_arr as $group) {
                        list($hash, $tag, $url) = $group;

                        if (preg_match('#src=("|\')(.*)("|\')#Usmi', $tag, $source)) {
                            $script = '<script type="text/javascript" ' . $defer . 'src="' . $url . '"></script>';
                        } else {
                            $inline_script = '';

                            $cacheMinify = new WpsolMinificationCache($hash, 'js');
                            if ($cacheMinify->check()) {
                                $inline_script = $cacheMinify->retrieve();
                            }
                            if (strlen($inline_script) > 0) {
                                $script = '<script type="text/javascript" ' . $defer . '>'.$inline_script.'</script>';
                            } else {
                                $script = '<script type="text/javascript" ' . $defer . 'src="' . $url . '"></script>';
                            }
                        }
                        $this->content = str_replace($tag, $script, $this->content);
                    }
                }
            }
        }

        //Inject External script
        if (!empty($this->external_local_url)) {
            foreach ($this->external_local_url as $group) {
                list($tag, $url) = $group;

                $script = '<script type="text/javascript" ' . $defer . 'src="' . $url . '"></script>';

                $this->content = str_replace($tag, $script, $this->content);
            }
        }


        // Restore COmments
        $this->content = $this->restoreComments($this->content);
        // Restore IE hacks
        $this->content = $this->restoreIEhacks($this->content);

        // Return the modified HTML
        return $this->content;
    }


    /**
     * Determines wheter a <script> $tag should be minify or not.
     *
     * We consider these as "aggregation-safe" currently:
     * - script tags without a `type` attribute
     * - script tags with an explicit `type` of `text/javascript`, 'text/ecmascript',
     *   'application/javascript' or 'application/ecmascript'
     * Everything else should return false.
     *
     * @param string $tag Tag to aggregate
     *
     * @return boolean
     *
     * original function by https://github.com/zytzagoo/ on his AO fork, thanks Tomas!
     */
    public function shouldMinify($tag)
    {
        preg_match('#<(script[^>]*)>#i', $tag, $scripttag);
        if (strpos($scripttag[1], 'type=') === false) {
            return true;
        } elseif (preg_match('/type=["\']?(?:text|application)\/(?:javascript|ecmascript)["\']?/i', $scripttag[1])) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get content of external script
     *
     * @param string $url External url
     *
     * @return mixed
     */
    public function getExternalData($url)
    {
        if (strpos($url, '//') === 0) {
            if (is_ssl()) {
                $http = 'https:';
            } else {
                $http = 'http:';
            }
            $url = $http . $url;
        }

        $data = '';
        $args = array(
            'httpversion' => '1.1',
        );
        //Start preload
        $response = wp_remote_get($url, $args);
        if (is_wp_error($response)) {
            $data = 'WPSOL_MINIFICATION; get content remotely failed!';
        }
        if (is_array($response)) {
            $data = $response['body'];
        }

        return $data;
    }
}
