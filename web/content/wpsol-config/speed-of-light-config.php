<?php 
defined( 'ABSPATH' ) || exit;
return array (
  'speed_optimization' => 
  array (
    'act_cache' => 1,
    'add_expires' => 1,
    'clean_cache' => 40,
    'clean_cache_each_params' => 2,
    'devices' => 
    array (
      'cache_desktop' => 1,
      'cache_tablet' => 1,
      'cache_mobile' => 2,
    ),
    'query_strings' => 1,
    'remove_rest_api' => 1,
    'remove_rss_feed' => 1,
    'cache_external_script' => 1,
    'disable_page' => 
    array (
      0 => '',
    ),
  ),
  'disable_page' => 
  array (
    0 => '/carrito-de-pedido',
    1 => '/pedido/*',
    2 => '',
  ),
  'homepage' => 'http://redlinks.com.ec/wordpress',
  'disable_per_adminuser' => 1,
  'disable_roles' => 
  array (
  ),
); 
